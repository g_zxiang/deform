package com.jjxliu.deform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.jjxliu.deform.pojo.UeEntry;

@Repository
public interface UeEntryDao {

	@Insert("insert into  entry(form_id,crtime,value,modify_time) values(#{e.form_id},#{e.crtime},#{e.value},#{e.modify_time})")
	@Options(useGeneratedKeys=true,keyProperty="e.id",keyColumn="id")
	public void insert(@Param("e") UeEntry e);
	
	@Select("select t1.* , t2.form_name from  entry as t1 , form as t2 where t1.form_id = t2.form_id  ")
	public List<UeEntry> findAll();
	
	
	@Select("select t1.* , t2.form_name from  entry as t1 , form as t2 where t1.form_id = t2.form_id and t1.id = #{id}")
	public UeEntry findById(@Param("id") int id);
	
	@Update("update  entry set modify_time=#{e.modify_time} , value=#{e.value} where id=#{e.id}")
	public void update(@Param("e") UeEntry e);
	
	@Delete("delete from  entry where id = #{entry_id}")
	public void delete(@Param("entry_id") int entry_id);
	
}
