<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>  
<meta charset="utf-8"> 
 
    <title>WEB表单设计器 Ueditor Formdesign Plugins -leipi.org</title>
      
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="author" content="leipi.org">
    <link href="${pageContext.request.contextPath}/res/css/bootstrap/css/bootstrap.css?2023" rel="stylesheet" type="text/css" />
 
    <link href="${pageContext.request.contextPath}/res/css/site.css?2023" rel="stylesheet" type="text/css" />
 	  

 </head>
<body>

 

<div class="container">
<form method="post" id="saveform" name="saveform" action="./save">
<input type="hidden" name="fields" id="fields" value="0">
<input type="hidden" name="formeditor" id="formeditor">
<input type="hidden" name="data" id="data">
<input type="hidden" name="form_id" id="form_id"   value='${form_id }'>
<input type="hidden" name="type" id="type"   value='1'>
<div class="row"> 
    	 
		<p style="margin-left: 20px;"> 表单名称: <input type="text" id="form_name" name="form_name" value="${form_name }"> </p>
    
</div>


<div class="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>提醒：</strong>单选框和复选框，如：<code>{|-</code>选项<code>-|}</code>两边边界是防止误删除控件，程序会把它们替换为空，请不要手动删除！
</div>


<div class="row">

<div class="span2">
<ul class="nav nav-list">
    <li class="nav-header">两栏布局</li>
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('text');" class="btn btn-link">文本框</a></li>
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('textarea');" class="btn btn-link">多行文本</a></li>
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('select');" class="btn btn-link">下拉菜单</a></li>
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('radios');" class="btn btn-link">单选框</a></li>
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('checkboxs');" class="btn btn-link">复选框</a></li> 
     <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('datepicker');" class="btn btn-link">日期选择框</a></li> 
    <li><a href="javascript:void(0);" onclick="leipiFormDesign.exec('listctrl');" class="btn btn-link">列表控件</a></li> 
    
</ul>

</div>

<div class="span10">

<script id="myFormDesign" type="text/plain" style="width:100%;">
	 ${form_template } 
</script>
</div>
 
</div><!--end row-->

</form>

	<div align="center">
		<button type="button" id="preview" class="btn btn-info">预览</button>
    	<button type="button" id="save" class="btn btn-info">保存</button>
	</div>

 	

</div><!--end container-->
 
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/jquery-1.7.2.min.js?2023"></script>

<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/ueditor.config.js?2023"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/ueditor.all.min.js?2023"> </script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/lang/zh-cn/zh-cn.js?2023"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/formdesign/leipi.formdesign.v4.js?2023"></script>
<!-- script start-->  
<script type="text/javascript" src="${pageContext.request.contextPath}/res/view/form/form_edit.js">
  
</script>
 
 
</body>
</html>